#include <stdio.h>
#include <crypt.h>
#include <unistd.h>

int main()
{
        char pid[16];
        snprintf(pid, sizeof(pid), "%i", getpid()+1);
        printf("%s\n", crypt(pid, "$1$awesome"));
}
